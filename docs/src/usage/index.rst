.. _operation_notebook:

.. raw:: html

    <style> .red {color:red} </style>

.. role:: red

Common Usage
===================

Deployment without X11
----------------------

Launch the ska-pst-dspsr docker image and mount the current working directory as `/mnt/data`

.. code-block:: bash

    docker run -it -v $(pwd):/mnt/data -w /mnt/data \
        artefact.skao.int/ska-pst-dspsr:0.0.13 bash


Deployment with X11
----------------------

To run applications that require X11 (e.g. PGPLOT), it is necessary to provide the X11 connection details on the docker command line,
as in the following examples.

Windows Subsystem for Linux (WSL2)
..................................

.. code-block:: bash

    docker run --rm -it -v $(pwd):/mnt/data -w /mnt/data \
        -v /tmp/.X11-unix:/tmp/.X11-unix -v /mnt/wslg:/mnt/wslg \
        -e DISPLAY -e WAYLAND_DISPLAY -e XDG_RUNTIME_DIR -e PULSE_SERVER \
        artefact.skao.int/ska-pst-dspsr:0.0.13 bash

Linux and FreeBSD
..................................

.. code-block:: bash

    docker run --rm -it -v $(pwd):/mnt/data -w /mnt/data \
        --net=host -e DISPLAY -v ${HOME}/.Xauthority:/home/pst/.Xauthority \
        artefact.skao.int/ska-pst-dspsr:0.0.13 bash

Mac running XQuartz
..................................

Before running docker, it may be necessary to install and/or configure XQuartz.

- Install XQuartz
- Open XQuartz -> Preferences -> Security
- Enable ``Allow connections from network clients``
- If necessary, restart XQuartz
- In an XQuartz terminal, run ``xhost + localhost``

With XQuartz installed and configured, run

.. code-block:: bash

    docker run --rm -it -v $(pwd):/mnt/data -w /mnt/data \
        -e DISPLAY=host.docker.internal:0 -v /tmp/.X11-unix:/tmp/.X11-unix \
        artefact.skao.int/ska-pst-dspsr:0.0.13 bash

Examples
===================

Simulated Square Wave
----------------------

After launching the ``ska-pst-dspsr`` docker container, download the following test data file; e.g.

.. code-block:: bash

    curl -O https://astronomy.swin.edu.au/pulsar/data/pst_rectangular_pulse.tgz

This file contains simulated output of the PST Voltage Recorder containaing normally distributed noise that
is amplitude-modulated by a square wave with a 20% duty cycle.  This simulation was generated as described
in the `ska-pst-common example uses <https://developer.skao.int/projects/ska-pst-common/en/latest/usage/index.html>`_.

Unzip the tarball to unpack the three files that it contains

.. code-block:: bash

    tar zxvf pst_rectangular_pulse.tgz

    data_and_weights.ls
    data/2023-08-05-11:07:09_0000000000000000_000000.dada
    weights/2023-08-05-11:07:09_0000000000000000_000000.dada

To compute the average of the rectangular pulses

.. code-block:: bash

    dspsr data_and_weights.ls

The output data file is named ``2023-08-05-11:07:09.ar`` and, if the docker container is launched with X11 configured,
the contents of this file can be plotted with

.. code-block:: bash

    psrplot -p freq+ -jp -j "r .5" 2023-08-05-11:07:09.ar -D /xs

This will plot a phase-vs-frequency image of the pulsed intensity, with the power from both polarisations added together,
like the following

.. image:: pst_rectangular_pulse.png
  :width: 800
  :alt: Intensity of a periodic rectangular wave as a function of pulse phase and simulated radio frequency

It's also possible to plot separate phase-vs-frequency images of the pulsed intensity for each polarisation; e.g.

.. code-block:: bash

    psrplot -p freq+ -l pol=0,1 -j "r .5" 2023-08-05-11:07:09.ar -D /xs

Note that the pulsed intensity varies linearly with frequency independently in each polarisation.


Pulsar Observed at Parkes
--------------------------

After launching the ``ska-pst-dspsr`` docker container, download the following test data file; e.g.

.. code-block:: bash

    curl -O https://astronomy.swin.edu.au/pulsar/data/1644-4559.dada

This file contains an observation of PSR J1644-4559 made with the Parkes Radiotelescope using the CPSR2 backend.
To see the header contents

.. code-block:: bash

    dada_edit 1644-4559.dada

Among other things, this header information shows that the observation was made at a centre frequency of 1405 MHz
using lower-sideband downconversion and a bandwidth of 64 MHz.  To perform phase-coherent dispersion removal
and compute the average pulse profile, use the convolving filterbank to simultaneously divide the band up into
128 frequency channels, and use four threads to speed things up.

.. code-block:: bash

    dspsr -F 128:D 1644-4559.dada -t4

The output data file is named ``2007-05-18-15:55:58.ar`` and, if the docker container is launched with X11 configured,
the contents of this file can be plotted with

.. code-block:: bash

    psrplot -pG -jp 2007-05-18-15:55:58.ar -D /xs

This will plot a phase-vs-frequency image of the pulsed intensity, with the power from both polarisations added together,
like the following

.. image:: 1644-4559_cpsr2.png
  :width: 800
  :alt: Intensity of PSR J1644-4559 as a function of pulse phase and radio frequency

Some of the things to notice in this plot include

- the dispersive delay, which varies with radio frequency;
- the narrow band of additional unpulsed flux near 1420.4 MHz, which is emitted by the neutral Hydrogen in our Galaxy along this line of sight; and
- the wide band of additional on-pulse quantization noise, known as scattered power, which is particularly visible at the edges of the band.
